from five import grok
from zope import schema
from zope.interface import implements
from z3c.relationfield.schema import RelationChoice
from Products.Five import BrowserView

from plone.directives import form, dexterity
from plone.dexterity.content import Container
from plone.formwidget.contenttree import ObjPathSourceBinder
from plone.app.textfield import RichText
from plone.namedfile.field import NamedImage

from tabellio.mixedcommittee import MessageFactory as _

class ICommittee(form.Schema):
    composition = RichText(title=_(u'Composition'), required=False)
    relationship = RichText(title=_(u'Relationship'), required=False)
    resolutions = RichText(title=_(u'Resolutions'), required=False)

class Committee(Container):
    implements(ICommittee)

    def events(self):
        events = [x for x in self.objectValues() if x.portal_type == 'tabellio.agenda.event']
        events.sort(lambda x,y: cmp(x.start, y.start))
        return events

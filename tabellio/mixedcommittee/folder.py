from Products.Five import BrowserView
from zope.interface import Interface


class ICommitteeFolder(Interface):
    pass

class CommitteeFolder(BrowserView):
    pass

